"use strict";
var __webpack_exports__ = {};
const external_url_namespaceObject = require("url");
const config = {
    api: {
        host: 'api.verifiedvisitors.com',
        /* VerifiedVisitors access token with "log" and "vac.verify" scopes */
        key: 'API_KEY',
    },
    logging: {
        path: '/log',
    },
    vac: {
        enabled: true,
        path: '/vac/verify',
        timeoutMs: 300,
    },
    vid: {
        cookieName: 'vv_vid',
        cookieExpirySeconds: 30 * 24 * 60 * 60,
    },
    captcha: {
        responseKey: 'h-captcha-response',
    },
    name: "vv-cfront",
    version: "1.0.0",
};

function getHeader(headers, key) {
    return headers[key]?.[0].value ?? undefined;
}
const external_https_namespaceObject = require("https");
const agent = new external_https_namespaceObject.Agent({
    keepAlive: true,
});

function http_request(opts) {
    return new Promise((resolve, reject) => {
        const request = {
            agent,
            host: opts.host,
            path: opts.path,
            method: opts.method ?? 'GET',
            headers: opts.headers ?? {},
        };
        if (opts.body !== undefined) {
            if (opts.method === 'GET' || opts.method === 'HEAD') {
                return reject('GET/HEAD request cannot have body');
            }
            request.headers['content-length'] = Buffer.byteLength(opts.body);
        }
        let timeout;
        let timedOut = false;
        const req = external_https_namespaceObject.request(request, (res) => {
            const chunks = [];
            res.on('error', (err) => {
                req.destroy();
                return reject(err);
            });
            res.on('data', (chunk) => {
                chunks.push(chunk);
            });
            res.on('end', () => {
                if (timeout) {
                    clearTimeout(timeout);
                }
                const body = chunks.length > 0
                    ? Buffer.concat(chunks).toString()
                    : undefined;
                const result = {
                    statusCode: res.statusCode,
                    headers: res.headers,
                    body,
                };
                req.destroy();
                resolve(result);
            });
        });
        req.on('error', (err) => {
            req.destroy();
            if (timedOut) {
                return reject(new Error('Request timed out'));
            }
            reject(err);
        });
        req.write(opts.body);
        req.end();
        if (opts.timeout) {
            timeout = setTimeout(() => {
                timedOut = true;
                req.destroy();
            }, opts.timeout);
        }
    });
}

async function query(request,
vid, requestTime) {
    const vacRequest = {
        timestamp: requestTime,
        visitorId: {
            ip: request.clientIp,
            ua: getHeader(request.headers, 'user-agent') ?? '',
            vid: vid ?? undefined,
        },
        host: getHeader(request.headers, 'host')?.replace(/^www\./, '') ?? '',
        uri: request.querystring !== ''
            ? `${request.uri}?${request.querystring}`
            : request.uri,
        method: request.method,
        headers: Object.keys(request.headers),
        connection: getHeader(request.headers, 'connection'),
        referer: getHeader(request.headers, 'referer'),
        origin: getHeader(request.headers, 'origin'),
        pragma: getHeader(request.headers, 'pragma'),
        xForwardedFor: getHeader(request.headers, 'x-forwarded-for'),
        xForwardedProto: getHeader(request.headers, 'x-forwarded-proto'),
        xRequestedWith: getHeader(request.headers, 'x-requested-with'),
        xRealIp: getHeader(request.headers, 'x-real-ip'),
        trueClientIp: getHeader(request.headers, 'true-client-ip'),
        via: getHeader(request.headers, 'via'),
        accept: getHeader(request.headers, 'accept'),
        acceptEncoding: getHeader(request.headers, 'acceptEncoding'),
        acceptLanguage: getHeader(request.headers, 'acceptLanguage'),
        acceptCharset: getHeader(request.headers, 'acceptCharset'),
        contentType: getHeader(request.headers, 'content-type'),
        contentLength: getHeader(request.headers, 'content-length'),
        cacheControl: getHeader(request.headers, 'cache-control'),
        from: getHeader(request.headers, 'from'),
        cc: getHeader(request.headers, 'cloudfront-viewer-country-name'),
        worker: {
            name: config.name,
            version: config.version,
        },
        log: true,
    };
    const t1 = Date.now();
    try {
        const res = await http_request({
            host: config.api.host,
            path: config.vac.path,
            method: 'POST',
            timeout: config.vac.timeoutMs,
            headers: {
                'content-type': 'application/json',
                'authorization': `bearer ${config.api.key}`,
            },
            body: JSON.stringify(vacRequest),
        });
        const vacFetchTimeMs = Date.now() - t1;
        if (res.statusCode !== 200) {
            return {
                action: 'allow',
                error: `Status ${res.statusCode}: ${res.body}`,
                vacFetchTimeMs,
            };
        }
        const decision = JSON.parse(res.body ?? '');
        return {
            ...decision,
            vacFetchTimeMs,
        };
    }
    catch (e) {
        console.error('An error occured while querying the visitor-access-control API: ', e);
        return {
            action: 'allow',
            error: `Error querying VerifiedVisitors: ${e.message ?? 'unknown error'}`,
            vacFetchTimeMs: Date.now() - t1,
        };
    }
}
//
//
//
//

function serveBlock() {
    const res = {
        status: '403',
        statusDescription: 'Forbidden',
    };
    return res;
}

function serveAllow(request) {
    return request;
}
//
//
//
//
//

function enforce(request,
vacResponse) {
    if (!config.vac.enabled) {
        return request;
    }
    switch (vacResponse.action) {
        case 'captcha':
            return {
                status: '403',
                statusDescription: 'Forbidden',
                headers: {
                    'content-type': [
                        {
                            key: 'Content-Type',
                            value: 'text/html;charset=utf-8',
                        },
                    ],
                    'vv-action': [{ key: 'Vv-Action', value: 'captcha' }],
                },
                body: vacResponse.htmlBody,
            };
        case 'js_challenge':
            return {
                status: '403',
                statusDescription: 'Forbidden',
                headers: {
                    'content-type': [
                        {
                            key: 'Content-Type',
                            value: 'text/html;charset=utf-8',
                        },
                    ],
                    'vv-action': [{ key: 'Vv-Action', value: 'js_challenge' }],
                },
                body: vacResponse.htmlBody,
            };
        case 'block':
            return serveBlock();
        case 'allow':
            return serveAllow(request);
    }
    return request;
}

function getCookieValues(headers, name) {
    const cookieHeaders = headers['cookie'];
    if (!cookieHeaders) {
        return [];
    }
    const matches = [];
    for (const cookieHeader of cookieHeaders) {
        const cookies = cookieHeader.value.split('; ');
        for (let i = 0; i < cookies.length; i++) {
            const kv = cookies[i].split('=');
            if (kv[0] === name) {
                matches.push(kv[1]);
            }
        }
    }
    return matches;
}

function getCookieValue(headers, name) {
    const cookies = getCookieValues(headers, name);
    return cookies[0] ?? null;
}

function buildCookie(name, value, opts) {
    const parts = [`${name}=${value}`, 'path=/', 'SameSite=Lax'];
    if (opts.domain) {
        parts.push(`domain=${opts.domain}`);
    }
    if (opts.expirySeconds) {
        const d = new Date();
        d.setSeconds(d.getSeconds() + opts.expirySeconds);
        parts.push(`expires=${d.toUTCString()}`);
    }
    return parts.join('; ');
}

function attachCookie(response, cookie) {
    if (!response.headers) {
        response.headers = {};
    }
    response.headers['set-cookie'] = [
        ...(response.headers['set-cookie'] ?? []),
        {
            key: 'set-cookie',
            value: cookie,
        },
    ];
    return response;
}

function isRequest(reqOrRes) {
    return 'clientIp' in reqOrRes;
}

function setVisitorIdCookie(res, vid, domain) {
    const vidCookie = buildCookie(config.vid.cookieName, vid, {
        domain,
        expirySeconds: config.vid.cookieExpirySeconds,
    });
    if (isRequest(res)) {
        /* Original request - redirect to set cookie */
        const redirectLoopKey = `_${config.vid.cookieName}`;
        const queryParams = new external_url_namespaceObject.URLSearchParams(res.querystring);
        if (!queryParams.has(redirectLoopKey)) {
            queryParams.set(redirectLoopKey, '1');
            res = {
                status: '302',
                statusDescription: 'Found',
                headers: {
                    location: [
                        {
                            key: 'Location',
                            value: `${res.uri}?${queryParams}`,
                        },
                    ],
                },
            };
        }
    }
    if (!isRequest(res)) {
        res = attachCookie(res, vidCookie);
    }
    return res;
}

/**
 * entrypoint
 */
const handler = async (event) => {
    const { request } = event.Records[0].cf;
    const requestTime = Date.now();
    try {
        const vid = getCookieValue(request.headers, config.vid.cookieName);
        const vacRes = await query(request, vid, requestTime);
        let res = enforce(request, vacRes);
        if (vacRes.visitorId !== undefined && vacRes.visitorId !== vid) {
            res = setVisitorIdCookie(res, vacRes.visitorId, vacRes.rootDomain);
        }
        return res;
    }
    catch (error) {
        console.error('An error occured while processing the request: ', error);
    }
    return request;
};
exports.handler = handler;
