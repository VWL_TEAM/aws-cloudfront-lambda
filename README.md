# VerifiedVisitors CloudFront Lambda@Edge integration

The VerifiedVisitors CloudFront integration is implemented as a Lambda@Edge
function executed at the viewer request event.

This repo contains the build output of the implementation.

---

## Setup

Please see our
[CloudFront integration docs](https://docs.verifiedvisitors.com/integrations/cloudfront/)
for detailed setup information.
